<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form method="POST" action="/welcome">
      @csrf
      <label for="fname">First name:</label><br />
      <input type="text" id="fname" name="fname" /><br /><br />
      <label for="lname">Last name:</label><br />
      <input type="text" id="lname" name="lname" /> <br /><br />
      Gender: <br />
      <input type="radio" id="male" name="gender" value="male" />
      <label for="male">Male</label><br />
      <input type="radio" id="female" name="gender" value="female" />
      <label for="female">Female</label><br />
      <input type="radio" id="other" name="gender" value="other" />
      <label for="other">Other</label><br /><br />
      Nationality: <br />
      <select name="dropdown">
        <option value="natInd" selected>Indonesian</option>
        <option value="natOther">Other</option>
      </select>
      <br /><br />
      Language Spoken: <br />
      <input type="checkbox" id="langInd" name="langInd" value="indonesia" />
      <label for="langInd"> Bahasa Indonesia</label><br />
      <input type="checkbox" id="langEng" name="langEng" value="english" />
      <label for="langEng"> English</label><br />
      <input type="checkbox" id="langOther" name="langOther" value="other" />
      <label for="langOther"> Other</label><br /><br />
      <label for="description">Bio:</label><br />
      <textarea
        id="description"
        rows="10"
        cols="30"
        name="description"
      ></textarea>
      <br /><br />
      <input type="submit" value="Sign Up" />
    </form>
</body>
</html>
